# Bullet Hell Survive Alpha

## About
A Bullet Hell gamedev tutorial for your browser, written in JavaScript and HTML. Working from the simplest game to something a lot more complex in small, manageable steps.

Read about the progress of this game at https://bhsalpha.blogspot.com/

## Running The Game
You can run the game in one of two ways:

1. Copy the contents of the source directory to a webserver. Browse the target directory on the webserver to run the game
2. Run the `server.py` script in the root directory. Browse to http://localhost:8000 to run the game

## More Info
Follow the blog on https://bhsalpha.blogspot.com

Contact me at evilpaul@evilpaul.org

See updates on Twitter https://twitter.com/evilpaul_atebit
