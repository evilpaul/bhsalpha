class StateManager {
	constructor() {
		this.stateStack = [];
		this.currentState = null;
	}
	pushState(state) {
		this.stateStack.push(state)
	}
	update() {
		if (this.currentState && this.currentState.isReadyToExit()) {
			logManager.info('Exiting state: ' + this.currentState.name)
			this.currentState.exit();
			this.currentState = null;
		}

		if (!this.currentState) {
			this.currentState = this.stateStack.shift();
			if (this.currentState) {
				logManager.info('Entering state: ' + this.currentState.name)
				this.currentState.enter();
			} else {
				logManager.fatal('No state to enter');
			}
		}

		if (this.currentState) {
			this.currentState.update();
		} else {
			logManager.fatal('No current state set');
		}
	}
	render() {
		if (this.currentState) {
			this.currentState.render();
		} else {
			logManager.fatal('No current state set');
		}
	}
}
