class Animator {
	constructor(numDataItems) {
		this.numDataItems = numDataItems;
		this.currentAnimationName = null;
		this.currentData = new Array(numDataItems).fill(null);
	}
	set(animationName) {
		if (animationName in animationsList) {
			this.currentAnimationName = animationName;
			this.jumpToFrame(0);
		} else {
			logManager.warning('Animation ' + this.animationName + ' not found');
			this.currentAnimationName = null;
		}
	}
	get() {
		return this.currentData;
	}
	getCurrentAnimationName() {
		return this.currentAnimationName;
	}
	jumpToFrame(frameNumber) {
		const animation = animationsList[this.currentAnimationName];
		const totalSteps = animation.steps.length;

		// Find the step that contains this frame
		let step = 0;
		let stepBase = 0;
		for (let i = 0; i < totalSteps - 1; i++) {
			const nextStepBase = stepBase + animation.steps[step].length;
			if (frameNumber < nextStepBase) {
				break;
			} else {
				stepBase = nextStepBase;
				step++;
			}
		}

		if (step == totalSteps - 1 && frameNumber == stepBase + animation.steps[step].length) {
			// We just get to the last frame
			// Is there a 'next' field?
			if ('next' in animation) {
				// Yes, jump to the next aniamtion
				this.set(animation.next);
			} else {
				// No, we're done
				this.frame = frameNumber;
			}
		} else if (step == totalSteps - 1 && frameNumber > stepBase + animation.steps[step].length) {
			// Animation is finished
			this.currentAnimationName = null;
		} else {
			// Set the data from the step
			const newDataItems = animation.steps[step].data;
			if (newDataItems.length == this.numDataItems) {
				this.currentData = newDataItems;
			} else {
				logManager.warning('Animation ' + this.currentAnimationName + ' step ' + step + ' has ' + newDataItems.length + ' items but ' + this.numDataItems + ' were expected');
			}

			// We are now at this frame number
			this.frame = frameNumber;
		}
	}
	update() {
		if (this.currentAnimationName) {
			this.jumpToFrame(this.frame);
			this.frame++;
		}
	}
}

