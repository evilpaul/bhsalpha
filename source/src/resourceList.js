// List of resources to load. Stored as an object where the Key is the resource id and Value is an
// object containing 'src' (filename) and 'type' (image, font, etc.)

const resourceList = {
	'font': {src:'font.png', type:'font'},

	'level_intro_countdown_001': {src:'level_intro_countdown_001.png', type:'image'},
	'level_intro_countdown_002': {src:'level_intro_countdown_002.png', type:'image'},
	'level_intro_countdown_003': {src:'level_intro_countdown_003.png', type:'image'},
	'level_intro_countdown_004': {src:'level_intro_countdown_004.png', type:'image'},

	'player': {src:'player.png', type:'image'},
	'player_hit': {src:'player_hit.png', type:'image'},
	'player_collision': {src:'player_collision.png', type:'collisionMap'},
	'player_graze': {src:'player_graze.png', type:'collisionMap'},

	'simple_bullet': {src:'simple_bullet.png', type:'image'},
	'simple_bullet_grazed': {src:'simple_bullet_grazed.png', type:'image'},
	'simple_bullet_collision': {src:'simple_bullet_collision.png', type:'collisionMap'},

	'score_bullet_inactive': {src:'score_bullet_inactive.png', type:'image'},
	'score_bullet_active': {src:'score_bullet_active.png', type:'image'},
	'score_bullet_collision': {src:'score_bullet_collision.png', type:'collisionMap'},

	'x_zapper_move': {src:'x_zapper_move.png', type:'image'},
	'x_zapper_charge': {src:'x_zapper_charge.png', type:'image'},
	'x_zapper_fire': {src:'x_zapper_fire.png', type:'image'},
	'x_zapper_fire_collision': {src:'x_zapper_fire_collision.png', type:'collisionMap'},

	'y_zapper_move': {src:'y_zapper_move.png', type:'image'},
	'y_zapper_charge': {src:'y_zapper_charge.png', type:'image'},
	'y_zapper_fire': {src:'y_zapper_fire.png', type:'image'},
	'y_zapper_fire_collision': {src:'y_zapper_fire_collision.png', type:'collisionMap'},

	'spawner_fire': {src:'spawner_fire.png', type:'image'},
	'spawner_rest': {src:'spawner_rest.png', type:'image'},
}
