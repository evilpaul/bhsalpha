class ObjectManager {
	constructor() {
		// These are the object groups that we know about
		this.objectGroups = ['spawner', 'player', 'enemy', 'bullet', 'scoreBullet'];

		// Create lists for each object type
		this.objectGroupLists = {};
		this.objectGroups.forEach(objectGroup => {
			this.objectGroupLists[objectGroup] = {};
		});

		// UID for objects
		this.objectId = 0;
	}
	reset() {
		this.objectGroups.forEach(objectGroup => {
			this.objectGroupLists[objectGroup] = {};
		});
	}
	update() {
		// Update all objects
		this.objectGroups.forEach(objectGroup => {
			const objectIds = Object.keys(this.objectGroupLists[objectGroup]);
			for (const objectId of objectIds) {
				const object = this.objectGroupLists[objectGroup][objectId]
				object.update();
			}
		});

		// Remove all dead objects
		this.objectGroups.forEach(objectGroup => {
			const objectIds = Object.keys(this.objectGroupLists[objectGroup]);
			for (const objectId of objectIds) {
				const object = this.objectGroupLists[objectGroup][objectId]
				if (!object.isAlive()) {
					delete this.objectGroupLists[objectGroup][objectId];
				}
			}
		});
	}
	render() {
		// Render all obejcts
		this.objectGroups.forEach(objectGroup => {
			const objectIds = Object.keys(this.objectGroupLists[objectGroup]);
			for (const objectId of objectIds) {
				const object = this.objectGroupLists[objectGroup][objectId]
				object.render();
			}
		});
	}
	collide(groupA, groupB, mapType) {
		// Collide every object of group a against every object of group b
		const objectAIds = Object.keys(this.objectGroupLists[groupA]);
		for (const objectAId of objectAIds) {
			const objectA = this.objectGroupLists[groupA][objectAId]
			const collisionMapA = resourceManager.getResource(objectA.collisionMaps[mapType]);
			if (collisionMapA) {
				const xA = objectA.xPos;
				const yA = objectA.yPos;
				const objectBIds = Object.keys(this.objectGroupLists[groupB]);
				for (const objectBId of objectBIds) {
					const objectB = this.objectGroupLists[groupB][objectBId]
					const collisionMapB = resourceManager.getResource(objectB.collisionMaps[mapType]);
					if (collisionMapB) {
						const xB = objectB.xPos;
						const yB = objectB.yPos;
						if (isColliding(xA, yA, collisionMapA, xB, yB, collisionMapB)) {
							// Notify both objects of collision
							objectA.onCollision(groupB, mapType);
							objectB.onCollision(groupA, mapType);
						}
					}
				}
			}
		}
	}
	add(object, id=null) {
		// Add object to the right group list
		const group = object.group;
		if (this.objectGroups.indexOf(group) != -1) {
			const newObjectId = id ? id : this.objectId++;
			this.objectGroupLists[group][newObjectId] = object;
			return newObjectId;
		} else {
			logManager.error('ObjectManger could not add to unknown object group "' + group + '"');
			return null;
		}
	}
	delete(objectId) {
		let [object, group] = this.findObject(objectId);
		if (object) {
			delete this.objectGroupLists[group][objectId];
		} else {
			logManager.warning('Could not find object with id: ' + objectId);
		}
	}
	getDetailsForAllObjectOfGroup(objectGroups) {
		let objectDetails = [];

		objectGroups.forEach(objectGroup => {
			const objectIds = Object.keys(this.objectGroupLists[objectGroup]);
			for (const objectId of objectIds) {
				const object = this.objectGroupLists[objectGroup][objectId]
				objectDetails.push({
					id: objectId,
					x: object.xPos,
					y: object.yPos,
					w: object.image.width,
					h: object.image.height,
				});
			}
		});

		return objectDetails;
	}
	getObjectConfigPosition(objectId) {
		let [object, group] = this.findObject(objectId);
		if (object) {
			return [object.config.x, object.config.y];
		} else {
			logManager.warning('Could not find object with id: ' + objectId);
			return [0, 0];
		}
	}
	setObjectPosition(objectId, x, y) {
		let [object, group] = this.findObject(objectId);
		if (object) {
			// Limit position to valid range
			const objectWidth = object.image.width; 
			const objectHeight = object.image.height; 
			x = clamp(x, -objectWidth / 2, globalConfig.canvasSize - objectWidth / 2);
			y = clamp(y, -objectHeight / 2, globalConfig.canvasSize - objectHeight / 2);

			// Set new position
			object.xPos = x;
			object.yPos = y;

			// Also update position in config
			object.config.x = x + object.image.width / 2;
			object.config.y = y + object.image.height / 2;
		} else {
			logManager.warning('Could not find object with id: ' + objectId);
		}
	}
	findObject(objectId) {
		let object = null;
		let group = null;
		this.objectGroups.forEach(objectGroup => {
			if (objectId in this.objectGroupLists[objectGroup]) {
				object = this.objectGroupLists[objectGroup][objectId];
				group = objectGroup;
			}
		});
		return [object, group];
	}
}
