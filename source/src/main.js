var canvas;
var conext2d;

var logManager = null;
var inputManager = null;
var objectManager = null;
var stateManager = null;
var resourceManager = null;

var currentScore = 0;
var bulletsGrazed = 0;
var hits = 0;
var chosenLevel = 0;


function init() {
	logManager = new LogManager();
	inputManager = new InputManager();
	objectManager = new ObjectManager();
	stateManager = new StateManager();
	resourceManager = new ResourceManager();

	canvas = document.getElementById('mainCanvas');
	if (!canvas) {
		logManager.fatal('Failed to find canvas');
		return;
	}
	context2d = canvas.getContext('2d');
	if (!this.context2d) {
		logManager.fatal('Couldn\'t get 2d context on canvas');
		return;
	}

	canvas.width = globalConfig.canvasSize;
	canvas.height = globalConfig.canvasSize;

	stateManager.pushState(new LoadingState());

	setInterval(mainLoop, globalConfig.msPerFrame);
}

function mainLoop() {
	inputManager.update();

	stateManager.update();
	stateManager.render();
}
