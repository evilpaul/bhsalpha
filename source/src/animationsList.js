const animationsList = {
	// Game ceremony
	'level_intro_countdown': {
		steps: [
			{ length:30, data:['level_intro_countdown_001'] },
			{ length:30, data:['level_intro_countdown_002'] },
			{ length:30, data:['level_intro_countdown_003'] },
			{ length:10, data:['level_intro_countdown_004'] },
			{ length:10, data:[null] },
			{ length:10, data:['level_intro_countdown_004'] },
			{ length:10, data:[null] },
			{ length:10, data:['level_intro_countdown_004'] },
			{ length:10, data:[null] },
			{ length:10, data:['level_intro_countdown_004'] },
			{ length:10, data:[null] },
		]
	},

	// Bullets
	'score_bullet_inactive': {
		steps: [
			{ length:1, data:['score_bullet_inactive', {}] },
		]
	},
	'score_bullet_active': {
		steps: [
			{ length:1, data:['score_bullet_active', {score:'score_bullet_collision'}] },
		],
	},
	'simple_bullet': {
		steps: [
			{ length:1, data:['simple_bullet', {hit:'simple_bullet_collision', score:'simple_bullet_collision'}] },
		]
	},
	'simple_bullet_grazed': {
		steps: [
			{ length:1, data:['simple_bullet_grazed', {hit:'simple_bullet_collision', score:'simple_bullet_collision'}] },
		]
	},

	// Zappers
	'x_zapper_inactive': {
		steps: [
			{ length:100, data:['x_zapper_move', {}] }
		],
		next: 'x_zapper_inactive'
	},
	'x_zapper_move': {
		steps: [
			{ length:230, data:['x_zapper_move', {}] },
		],
		next: 'x_zapper_fire'
	},
	'x_zapper_fire': {
		steps: [
			{ length:40, data:['x_zapper_charge', {}] },
			{ length:60, data:['x_zapper_fire', {hit:'x_zapper_fire_collision'}] },
			{ length:40, data:['x_zapper_charge', {}] },
		],
		next: 'x_zapper_move'
	},
	'y_zapper_inactive': {
		steps: [
			{ length:100, data:['y_zapper_move', {}] }
		],
		next: 'y_zapper_inactive'
	},
	'y_zapper_move': {
		steps: [
			{ length:230, data:['y_zapper_move', {}] },
		],
		next: 'y_zapper_fire'
	},
	'y_zapper_fire': {
		steps: [
			{ length:40, data:['y_zapper_charge', {}] },
			{ length:60, data:['y_zapper_fire', {hit:'y_zapper_fire_collision'}] },
			{ length:40, data:['y_zapper_charge', {}] },
		],
		next: 'y_zapper_move'
	},

	// Spawners
	'spawner_start': {
		steps: [
			{ length:1, data:['spawner_rest', {}] },
		]
	},
	'spawner_fire': {
		steps: [
			{ length:4, data:['spawner_fire', {}] },
			{ length:1, data:['spawner_rest', {}] },
		]
	},

	// Player ships
	'player': {
		steps: [
			{ length:1, data:['player', {hit:'player_collision', score:'player_graze'}] },
		]
	},
	'player_hit': {
		steps: [
			{ length:60, data:['player_hit', {}] },
		],
		next: 'player'
	},
}
