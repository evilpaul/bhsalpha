class ResourceManager {
	constructor() {
		// Counts the number of loads in progress
		this.loadsPending = 0;

		// This is where we store the resources once they are loaded
		this.availableResources = {}

		// Load all resource from the resourceList
		for (const id of Object.keys(resourceList)) {
			let resource = resourceList[id];
			const url = 'data/' + resource.src;
			logManager.info('ResourceManager loading resource ' + id + ' from ' + url);
			this.loadsPending++;

			// Create an XMLHttp request to get the resource data
			let _this = this;
			let request = new XMLHttpRequest();
			request.open('GET', url);
			request.responseType = 'arraybuffer';
			request.onreadystatechange = () => {
				// This is called when the request changes state. State 4 means that the request
				// is finished. Other states aren't important to us
				if (request.readyState == 4) {
					logManager.info('ResourceManager loading resource ' + id + ' completed, response: ' + request.status + ' ' + request.statusText);
					if (request.status == 200 || request.state == 304) {
						// Create resource data according to its type
						const resourceType = resource.type;
						const handler = resourceTypeHandlers[resourceType];
						if (handler) {
							const binaryData = new Uint8Array(request.response);
							handler(binaryData, (result) => {
								// Save the transformed data
								_this.availableResources[id] = result;
								// One less load to do..
								_this.loadsPending--;
							});
						} else {
							logManager.error('ResourceManager has no handler for resource ' + id + ', type ' + resourceType);
							_this.availableResources[id] = null;
							_this.loadsPending--;
						}
					} else {
						logManager.error('ResourceManager failed to load resource ' + id);
						_this.availableResources[id] = null;
						_this.loadsPending--;
					}
				}
			}
			request.send(null);
		}
	}
	areAllResourcesLoaded() {
		return this.loadsPending == 0;
	}
	getResource(id) {
		if (id == null) {
			return null;
		} else if (id in this.availableResources) {
			return this.availableResources[id];
		} else {
			logManager.error('Resource id ' + id + ' unknown or not loaded');
			return null;
		}
	}
	isResourceAvailable(id) {
		return id in this.availableResources;
	}
}
