const levelSchema = {
	required: ['type', 'version', 'meta', 'objects'],
	properties: {
		type: {string:'integer', pattern:'BHS Map'},
		version: {type:'integer', enum:[1]},
		meta: {
			type: 'object',
			required: ['name', 'targetScore', 'targetHits'],
			properties: {
				name: {type:'string', minLength:1, maxLength:32},
				targetScore: {type:'integer', minimum:0},
				targetHits: {type:'integer', minimum:0},
			},
			additionalProperties: false,
		},
		objects: {
			type: 'object',
			required: ['player'],
			properties: {
				player: {type: 'array', items: PlayerObject.schema(), minItems:1, maxItems:1},
				zapper: {type: 'array', items: ZapperObject.schema()},
				bulletSpawner: {type: 'array', items: BulletSpawnerObject.schema()},
			},
			additionalProperties: false,
		},
		additionalProperties: false,
	},
	additionalProperties: false,
};


class LevelLoader {
	constructor() {
		this.levelName = 'NOT SET';
		this.targetScore = 1000;
		this.targetHits = 2;
		this.isLoaded = false;
	}
	load(levelData) {
		// Don't use the same loader object twice
		if (this.isLoaded) {
			logManager.warning('Trying to load level more than once');
			return false;
		}

		// Validate that the data matches the schema
		const validator = jsen(levelSchema);
		if (!validator(levelData)) {
			logManager.error('Level date failed validation');
			validator.errors.forEach((error) => {
				logManager.info('Validation error: ' + JSON.stringify(error));
			});
			return false;
		} else {
			logManager.info('Level data passed validation');
		}

		// Load the meta data
		this.levelName = levelData.meta.name;
		this.targetScore = levelData.meta.targetScore;
		this.targetHits = levelData.meta.targetHits;
		logManager.info('Loading level \'' + this.levelName + '\'');

		// Load all valid object types
		const validObjectsClassesToLoad = [PlayerObject, ZapperObject, BulletSpawnerObject];	
		validObjectsClassesToLoad.forEach((objectClass) => {
			const objectTypeName = objectClass.typeName();
			if (levelData.objects[objectTypeName]) {
				logManager.info('Loading \'' + objectTypeName + '\' objects');
				levelData.objects[objectTypeName].forEach((config) => {
					let newObject = new objectClass(config);
					objectManager.add(newObject);
				});
			}
		});

		logManager.info('Finished loading level');
		this.isLoaded = true;
		return true;
	}
}
