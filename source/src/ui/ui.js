let GUIfillStyles = {
	'window_title_bar_outline': '#000',
	'window_background':		'#222',
	'window_title_text':		'#fff',
	'window_title_bar_normal':	'#333',
	'window_title_bar_hover':	'#444',
	'window_title_bar_pressed':	'#555',

	'group_outline':			'#333',

	'button_normal':			'#333',
	'button_hover':				'#444',
	'button_pressed':			'#555',

	'field_normal':				'#256',
	'field_hover':				'#367',
	'field_active':				'#478',

	'checkbox_normal_on':		'#030',
	'checkbox_normal_off':		'#300',
	'checkbox_hover_on':		'#040',
	'checkbox_hover_off':		'#400',
	'checkbox_pressed_on':		'#050',
	'checkbox_pressed_off':		'#500',

	'scrollbar_full_normal':	'#080',
	'scrollbar_full_hover':		'#282',
	'scrollbar_full_active':	'#4d4',
	'scrollbar_empty_normal':	'#800',
	'scrollbar_empty_hover':	'#822',
	'scrollbar_empty_active':	'#d44',
};


class GUI {
	constructor(canvas, context2d) {
		this.canvas = canvas;
		this.context2d = context2d;

		// Input data
		this.mouseX = 0;
		this.mouseY = 0;
		this.mouseDown = false;
		this.mouseDoubleClick = false;
		this.keysDown = new Set();
		this.inputConsumed = false;

		// Input handlers
		this.newMouseX = 0;
		this.newMouseY = 0;
		this.newMouseDown = false;
		this.newMouseDoubleClick = false;
		this.newKeysDown = new Set();
		document.addEventListener('mousemove', (event) => {
			const canvasRect = canvas.getBoundingClientRect();
			this.newMouseX = event.x - canvasRect.left - 10;
			this.newMouseY = event.y - canvasRect.top - 10;
		});
		document.addEventListener('mousedown', (event) => {
			this.newMouseDown = true;
		});
		document.addEventListener('mouseup', (event) => {
			this.newMouseDown = false;
		});
		document.addEventListener('dblclick', (event) => {
			this.newMouseDoubleClick = true;
		});
		document.addEventListener('keydown', (event) => {
			this.newKeysDown.add(event.key);
		});
		document.addEventListener('keyup', (event) => {
			this.newKeysDown.delete(event.key);
		});

		this.windowInfo = {};
		this.windowZOrder = 0;
		this.groupInfo = {};
		this.timelineInfo = {};		// should be moved or renamed

		this.active = null;

		this.frameCount = 0;

		this.renderer = new GUIRenderer();
	}

	startFrame() {
		this.mouseDeltaX = this.newMouseX - this.mouseX;
		this.mouseDeltaY = this.newMouseY - this.mouseY;
		this.mouseX = this.newMouseX;
		this.mouseY = this.newMouseY;
		this.mouseJustDown = this.mouseDown == false && this.newMouseDown == true;
		this.mouseJustUp = this.mouseDown == true && this.newMouseDown == false;
		this.mouseDown = this.newMouseDown;
		this.mouseDoubleClick = this.newMouseDoubleClick;

		this.keysJustDown = new Set(this.newKeysDown); for (var x of this.keysDown) {this.keysJustDown.delete(x);}
		this.keysDown = new Set(this.newKeysDown);

		this.inputConsumed = false;

		this.items = [];
		this.windowZOrderedItems = {};
		this.x = 10;
		this.y = 10;
		this.w = 100;
		this.parents = [];
		this.idPath = [];
		this.idPathExpanded = '';

		this.frameCount++;
	}

	wasInputConsumed() {
		return this.inputConsumed;
	}

	renderFrame() {
		this.renderer._render(this.context2d, this.windowZOrderedItems);
	}

	beginWindow(title, closeButton = false, width = 250) {
		const splitTitle = this._splitLabel(title);

		let action = null;
		let x = this.x + 25;
		let y = this.y + 25;
		const w = width;
		const h = 20;

		const id = splitTitle.id;

		if (id in this.windowInfo) {
			x = this.windowInfo[id].x;
			y = this.windowInfo[id].y;
			x = Math.min(Math.max(0, x), this.canvas.width - width);
			y = Math.min(Math.max(0, y), this.canvas.height - 20);
			this.windowInfo[id].x = x;
			this.windowInfo[id].y = y;
		} else {
			this.windowInfo[id] = {
				x: x,
				y: y,
				zOrder: ++this.windowZOrder,
				isExpanded: true,
			};
		}
		const zOrder = this.windowInfo[id].zOrder;

		// Is this window covered by another one at the current mouse position?
		this.canTakeMouseInput = true;
		for (let i in this.windowInfo) {
			const windowInfo = this.windowInfo[i];
			if (id != i) {
				if (zOrder < windowInfo.zOrder) {
					if (this.mouseX >= windowInfo.x && this.mouseX <= windowInfo.x + windowInfo.w && this.mouseY >= windowInfo.y && this.mouseY <= windowInfo.y + windowInfo.h) {
						this.canTakeMouseInput = false;
						break;
					}
				}
			}
		}

		// Minimise button
		this._pushId(splitTitle.id);
		let minimiseButtonItem = {
			x: x + w - 15,
			y: y,
			w: 15,
			h: 15,
		}
		if (this._buttonStuff(minimiseButtonItem, 'button', this.windowInfo[id].isExpanded?'-':'+')) {
			this.windowInfo[id].isExpanded = !this.windowInfo[id].isExpanded;
		}
		this._popId(splitTitle.id);

		// Close button
		let closeButtonItem;
		if (closeButton) {
			this._pushId(splitTitle.id);
			closeButtonItem = {
				x: x + w - 15 - 15,
				y: y,
				w: 15,
				h: 15,
			}
			if (this._buttonStuff(closeButtonItem, 'button', 'X')) {
				action = 'closed';
			}
			this._popId(splitTitle.id);
		}

		let style = 'normal';
		if (!(this.active && this.active != id)) {
			const inside = this.canTakeMouseInput && this.mouseX >= x && this.mouseX <= x + w - 15 - (closeButton?15:0) && this.mouseY >= y && this.mouseY < y + h;
			if (inside) {
				style = 'hover';
				if (this.mouseJustDown) {
					if (this.windowInfo[id].zOrder != this.windowZOrder) {
						this.windowInfo[id].zOrder = ++this.windowZOrder;
					}
					this.active = id;
				}
			}

			if (id == this.active) {
				style = 'pressed';
				if (this.mouseJustUp) {
					this.active = null;
					style = 'normal';
				} else {
					this.windowInfo[id].x += this.mouseDeltaX;
					this.windowInfo[id].y += this.mouseDeltaY;
				}
				this.inputConsumed = true;
			}
		}

		let newWindow = {
			type: 'window',
			x: x,
			y: y,
			w: w,
			h: h,
			title: splitTitle.display,
			style: style,
			isExpanded: this.windowInfo[id].isExpanded,
			zOrder: zOrder,
		};

		this.x = newWindow.x + 5;
		this.y = newWindow.y + 20;
		this.w = newWindow.w - 5 * 2;

		this.items.push(newWindow);
		this.items.push(minimiseButtonItem);
		if (closeButton) this.items.push(closeButtonItem);
		this.parents.push(newWindow);

		this._pushId(splitTitle.id);

		return action;
	}
	endWindow() {
		const parent = this.parents.pop();
		parent.h = this.y - parent.y;

		this.windowZOrderedItems[parent.zOrder] = this.items;
		this.items = [];

		this.windowInfo[this.idPath].w = parent.w;
		this.windowInfo[this.idPath].h = parent.h;

		if (this.mouseX >= parent.x && this.mouseX < parent.x + parent.w &&
			this.mouseY >= parent.y && this.mouseY < parent.y + parent.h) {
			this.inputConsumed = true;
		}

		this._popId();
	}

	beginGroup(label, startExpanded=true) {
		const splitLabel = this._splitLabel(label);

		this._pushId(splitLabel.id);
		if (this._isExpanded()) {
			const id = [this.idPathExpanded, splitLabel.id].join();

			if (!(id in this.groupInfo)) {
				this.groupInfo[id] = {
					isExpanded: startExpanded,
				};
			}

			let item = this._positionStuff();
			item.type = 'group';
			item.label = splitLabel.display;
			item.isExpanded = this.groupInfo[id].isExpanded;
			this.items.push(item);

			let buttonItem = {
				x:item.x+item.w-15,
				y:item.y,
				w:15,
				h:15,
			}
			if (this._buttonStuff(buttonItem, 'button', this.groupInfo[id].isExpanded?'-':'+')) {
				this.groupInfo[id].isExpanded = !this.groupInfo[id].isExpanded;
			}
			this.items.push(buttonItem);
			this.parents.push(item);

			this.x += 5;
			this.w -= 10;
		} else {
			this.parents.push({isExpanded: false});
		}
		return this._isExpanded();
	}
	endGroup() {
		const parent = this.parents.pop();
		if (parent.isExpanded) {
			parent.h = this.y - parent.y;
			this.y += 5;
		}
		this.x -= 5;
		this.w += 10;
		this._popId();
	}

	hotkey(key) {
		if (this.active == null) {
			if (this.keysJustDown.has(key)) {
				this.keysJustDown.delete(key);
				this.inputConsumed = true;
				return true;
			}
		}
		return false;
	}

	label(label) {
		if (this._isExpanded()) {
			let item = this._positionStuff();
			item.type = 'label';
			item.label = label;
			this.items.push(item);
		}
	}
	scrollbar(label, value, min, max) {
		value = Math.max(Math.min(value, max), min);
		if (this._isExpanded()) {
			let item = this._positionStuff();
			item.type = 'scrollbar';

			const id = [this.idPathExpanded, label].join();
			item.style = 'normal';
			if (!(this.active && this.active != id)) {
				const inside = this.canTakeMouseInput && this.mouseX >= item.x && this.mouseX <= item.x + item.w && this.mouseY >= item.y && this.mouseY < item.y + item.h;
				if (id == this.active) {
					item.style = 'active';
					const mouseAlpha = Math.max(Math.min((this.mouseX - item.x) / this.w, 1), 0);
					value = min + mouseAlpha * (max - min); 
					if (this.mouseJustUp) {
						this.active = null;
					}
				} else if (inside) {
					item.style = 'hover';
					if (this.mouseJustDown) {
						this.active = id;
					}
				}
			}
			item.fillAlpha = (value - min) / (max - min);
			this.items.push(item);
		}
		return value;
	}
	button(label) {
		if (this._isExpanded()) {
			let item = this._positionStuff();
			const clicked = this._buttonStuff(item, 'button', label);
			this.items.push(item);
			return clicked;
		} else {
			return false;
		}
	}
	checkbox(label, state) {
		if (this._isExpanded()) {
			let item = this._positionStuff();
			const clicked = this._buttonStuff(item, 'checkbox', label);
			item.style = item.style + (state ? '_on' : '_off');
			this.items.push(item);
			return clicked ^ state;
		} else {
			return state;
		}
	}
	intValue(label, value, min, max) {
		if (this._isExpanded()) {
			this._pushId(label);

			// Field
			value = this._fieldStuff(label, value, 5, 12, 5, '-0123456789',
				(value) => {
					return value.toString();
				},
				(inputField) => {
					let newValue = parseInt(inputField);
					if (!isNaN(newValue)) {
						return Math.max(Math.min(newValue, max), min);
					} else {
						return null;
					}
				}
			);

			// +/- buttons
			let buttonItem = this._positionStuff(10, 12, 1, false);
			if (this._buttonStuff(buttonItem, 'button', '-')) {
				value = Math.max(value - 1, min);
			}
			this.items.push(buttonItem);
			buttonItem = this._positionStuff(11, 12, 1, false);
			if (this._buttonStuff(buttonItem, 'button', '+')) {
				value = Math.min(value + 1, max);
			}
			this.items.push(buttonItem);

			// Label
			let labelItem = this._positionStuff(0, 12, 5, true);
			labelItem.type = 'label';
			labelItem.label = label;
			this.items.push(labelItem);

			this._popId();
		}
		return value;
	}
	floatValue(label, value, min, max) {
		if (this._isExpanded()) {
			this._pushId(label);

			// Field
			value = this._fieldStuff(label, value, 5, 12, 5, '-0123456789.',
				(value) => {
					return value.toString();
				},
				(inputField) => {
					let newValue = parseFloat(inputField);
					if (!isNaN(newValue)) {
						return Math.max(Math.min(newValue, max), min);
					} else {
						return null;
					}
				}
			);

			// +/- buttons
			let buttonItem = this._positionStuff(10, 12, 1, false);
			if (this._buttonStuff(buttonItem, 'button', '-')) {
				value = Math.max(value - 1, min);
			}
			this.items.push(buttonItem);
			buttonItem = this._positionStuff(11, 12, 1, false);
			if (this._buttonStuff(buttonItem, 'button', '+')) {
				value = Math.min(value + 1, max);
			}
			this.items.push(buttonItem);

			// Label
			let labelItem = this._positionStuff(0, 12, 5, true);
			labelItem.type = 'label';
			labelItem.label = label;
			this.items.push(labelItem);

			this._popId();
		}
		return value;
	}
	float2Value(label, value1, min1, max1, value2, min2, max2) {
		if (this._isExpanded()) {
			this._pushId(label);

			// Field 1
			value1 = this._fieldStuff('1-' + label, value1, 5, 12, 3.5, '-0123456789.',
				(value) => {
					return value.toString();
				},
				(inputField) => {
					let newValue = parseFloat(inputField);
					if (!isNaN(newValue)) {
						return Math.max(Math.min(newValue, max1), min1);
					} else {
						return null;
					}
				}
			);

			// Field 2
			value2 = this._fieldStuff('2-' + label, value2, 8.5, 12, 3.5, '-0123456789.',
				(value) => {
					return value.toString();
				},
				(inputField) => {
					let newValue = parseFloat(inputField);
					if (!isNaN(newValue)) {
						return Math.max(Math.min(newValue, max2), min2);
					} else {
						return null;
					}
				}
			);

			// Label
			let labelItem = this._positionStuff(0, 12, 5, true);
			labelItem.type = 'label';
			labelItem.label = label;
			this.items.push(labelItem);

			this._popId();
		}
		return [value1, value2];
	}
	stringValue(label, value) {
		if (this._isExpanded()) {
			this._pushId(label);

			// Field
			value = this._fieldStuff(label, value, 5, 12, 7, '-0123456789.abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXZ',
				(value) => {
					return value;
				},
				(inputField) => {
					return inputField;
				}
			);

			// Label
			let labelItem = this._positionStuff(0, 12, 5, true);
			labelItem.type = 'label';
			labelItem.label = label;
			this.items.push(labelItem);

			this._popId();
		}
		return value;
	}
	listValue(label, value, options) {
		if (this._isExpanded()) {
			this._pushId(label);
			let valuePosition = options.indexOf(value);
			const optionCount = options.length;

			// Field
			let fieldItem = this._positionStuff(5, 12, 5, false);
			fieldItem.type = 'field';
			fieldItem.label = value;
			fieldItem.style = 'normal';
			this.items.push(fieldItem);
			if (!this.active) {
				const inside = this.canTakeMouseInput && this.mouseX >= fieldItem.x && this.mouseX <= fieldItem.x + fieldItem.w && this.mouseY >= fieldItem.y && this.mouseY < fieldItem.y + fieldItem.h;
				if (inside) {
					fieldItem.style = 'hover';
					if (this.mouseJustDown) {
						valuePosition = (valuePosition + 1 + optionCount) % optionCount;
					}
				}
			}

			// Up/down buttons
			let buttonItem = this._positionStuff(10, 12, 1, false);
			if (this._buttonStuff(buttonItem, 'button', '<')) {
				valuePosition = (valuePosition - 1 + optionCount) % optionCount;
			}
			this.items.push(buttonItem);
			buttonItem = this._positionStuff(11, 12, 1, false);
			if (this._buttonStuff(buttonItem, 'button', '>')) {
				valuePosition = (valuePosition + 1 + optionCount) % optionCount;
			}
			this.items.push(buttonItem);
			value = options[valuePosition];

			// Label
			let labelItem = this._positionStuff(0, 12, 5, true);
			labelItem.type = 'label';
			labelItem.label = label;
			this.items.push(labelItem);

			this._popId();
		}
		return value;
	}
	rowButtons(labels) {
		let action = null;
		if (this._isExpanded()) {
			this._pushId(labels.join());

			const buttonCount = labels.length;
			for (let i = 0; i < buttonCount; i++) {
				let item = this._positionStuff(i, buttonCount, 1, i == (buttonCount - 1));
				if (this._buttonStuff(item, 'button', labels[i])) {
					action = i;
				}
				this.items.push(item);
			}

			this._popId();
		}
		return action;
	}
	radioButtons(labels, selection) {
		if (this._isExpanded()) {
			this._pushId(labels.join());

			const buttonCount = labels.length;
			for (let i = 0; i < buttonCount; i++) {
				let item = this._positionStuff(i, buttonCount, 1, i == (buttonCount - 1));
				if (this._buttonStuff(item, 'checkbox', labels[i])) {
					selection = i;
				}
				item.style = item.style + ((i == selection) ? '_on' : '_off');
				this.items.push(item);
			}

			this._popId();
		}
		return selection;
	}

	_pushId(id) {
		this.idPath.push(id);
		this.idPathExpanded = this.idPath.join();
	}
	_popId() {
		this.idPath.pop();
		this.idPathExpanded = this.idPath.join();
	}
	_isExpanded() {
		if (this.parents.length) {
			return this.parents[this.parents.length-1].isExpanded;
		} else {
			return false;
		}
	}
	// invisible^visble$variable
	// id + display
	_splitLabel(labelText) {
		return {
			id: labelText.replace(/\$.*/, '').replace(/\^/, ''),
			display: labelText.replace(/.*\^/, '').replace(/\$/, ''),
		};
	}
	_positionStuff(item=0, items=1, width=1, newline=true) {
		const totalW = this.w;
		const itemW = (totalW - ((items - 1) * 5)) / items;
		const x = this.x + (item * (itemW + 5));
		const w = Math.floor(itemW * width + (width - 1) * 5);

		const y = this.y;
		const h = 15;
		if (newline) {
			this.y += h + 5;
		}

		return {
			x: x,
			y: y,
			w: w,
			h: h,
		}
	}
	_buttonStuff(item, type, label) {
		item.style = 'normal';
		item.type = type;
		item.label = label;
		let clicked = false;

		const id = [this.idPathExpanded, label].join();

		if (!(this.active && this.active != id)) {
			const inside = this.canTakeMouseInput && this.mouseX >= item.x && this.mouseX <= item.x + item.w && this.mouseY >= item.y && this.mouseY < item.y + item.h;

			if (id == this.active) {
				if (inside) {
					item.style = 'pressed';
					if (this.mouseJustUp) {
						this.active = null;
						clicked = true;
					}
				} else if (this.mouseJustUp) {
					this.active = null;
				}
			} else if (inside) {
				item.style = 'hover';
				if (this.mouseJustDown) {
					this.active = id;
				}
			}
		}

		return clicked;
	}
	_fieldStuff(label, value, itemPos, itemCount, itemWidth, possibleChars, read, write) {
		// Value field
		const id = [this.idPathExpanded, label].join();
		let fieldItem = this._positionStuff(itemPos, itemCount, itemWidth, false);
		fieldItem.type = 'field';
		fieldItem.label = value;
		fieldItem.style = 'normal';
		if (!(this.active && this.active != id)) {
			const inside = this.canTakeMouseInput && this.mouseX >= fieldItem.x && this.mouseX <= fieldItem.x + fieldItem.w && this.mouseY >= fieldItem.y && this.mouseY < fieldItem.y + fieldItem.h;
			if (id == this.active) {
				this.inputConsumed = true;
				fieldItem.style = 'active';
				for (let i = 0; i < possibleChars.length; i++) {
					const possibleChar = possibleChars[i];
					if (this.keysJustDown.has(possibleChar)) {
						this.keysJustDown.delete(possibleChar);
						this.inputField += possibleChar;
					}
				}
				if (this.keysJustDown.has('Enter')) {
					this.keysJustDown.delete('Enter');
					let newValue = write(this.inputField);
					if (newValue !== null) {
						value = newValue;
					}
					this.active = null;
				} else if (this.keysJustDown.has('Escape')) {
					this.keysJustDown.delete('Escape');
					this.active = null;
				} else if (this.keysJustDown.has('Backspace')) {
					this.keysJustDown.delete('Backspace');
					this.inputField = this.inputField.slice(0, -1);
				}
				fieldItem.label = this.inputField;
				if (this.frameCount & 16) {
					fieldItem.label += '|';
				}
			} else if (inside) {
				fieldItem.style = 'hover';
				if (this.mouseJustDown) {
					this.active = id;
					this.inputField = read(value);
				}
			}
		}
		this.items.push(fieldItem);
		return value;
	}
}


class GUIRenderer {
	_render(context2d, windows) {
		context2d.font = '11px ' + 'Verdana';
		context2d.textAlign = 'left';
		context2d.textBaseline = 'middle';
		Object.values(windows).forEach(items => {
			items.forEach(item => {
				this[item.type](context2d, item);
			});
		});
	}
	
	label(context2d, item) {
		context2d.fillStyle = '#fff';
		context2d.fillText(item.label, item.x, item.y + item.h / 2);
	}	
	button(context2d, item) {
		context2d.fillStyle = GUIfillStyles['button_' + item.style]
		context2d.fillRect(item.x, item.y, item.w, item.h);
		context2d.fillStyle = '#fff';
		const textOffset = item.style == 'pressed' ? 1 : 0;
		context2d.textAlign = 'center';
		context2d.fillText(item.label, item.x + item.w / 2 + textOffset, item.y + item.h / 2 + textOffset);
		context2d.textAlign = 'left';
	}
	checkbox(context2d, item) {
		context2d.fillStyle = GUIfillStyles['checkbox_' + item.style];
		context2d.fillRect(item.x, item.y, item.w, item.h);
		context2d.fillStyle = '#fff';
		const textOffset = item.style.startsWith('pressed') ? 1 : 0;
		context2d.textAlign = 'center';
		context2d.fillText(item.label, item.x + item.w / 2 + textOffset, item.y + item.h / 2 + textOffset);
		context2d.textAlign = 'left';
	}
	field(context2d, item) {
		context2d.fillStyle = GUIfillStyles['field_' + item.style];
		context2d.fillRect(item.x, item.y, item.w, item.h);
		context2d.fillStyle = '#fff';
		context2d.font = '11px ' + 'Consolas';
		context2d.fillText(item.label, item.x + 5, item.y + item.h / 2);
		context2d.font = '11px ' + 'Verdana';
	}
	group(context2d, item) {
		context2d.fillStyle = GUIfillStyles['group_outline'];
		context2d.fillRect(item.x, item.y, item.w, item.h);
		context2d.fillStyle = GUIfillStyles['window_background'];
		context2d.fillRect(item.x+1, item.y+15, item.w-2, item.h-16);
		context2d.fillStyle = '#fff';
		context2d.fillText(item.label, item.x + 5, item.y + 15 / 2);
	}
	scrollbar(context2d, item) {
		context2d.fillStyle = GUIfillStyles['scrollbar_full_' + item.style];
		context2d.fillRect(item.x, item.y, item.w, item.h);
		const fillLength = item.fillAlpha * item.w;
		context2d.fillStyle = GUIfillStyles['scrollbar_empty_' + item.style];
		context2d.fillRect(item.x + fillLength, item.y, item.w - fillLength, item.h);
	}
	window(context2d, item) {
		// Outline (with shadow)
		context2d.shadowBlur = 6;
		context2d.shadowColor = 'rgba(0,0,0,0.5)';
		context2d.shadowOffsetX = 3;
		context2d.shadowOffsetY = 3;
		context2d.fillStyle = GUIfillStyles['window_title_bar_outline'];
		context2d.fillRect(item.x - 2, item.y - 2, item.w + 4, item.h + 4);
		context2d.shadowBlur = 0;
		context2d.shadowColor = 'transparent';

		// Title bar
		context2d.fillStyle = GUIfillStyles['window_title_bar_' + item.style];
		context2d.fillRect(item.x, item.y, item.w, 15);

		// Title
		context2d.fillStyle = GUIfillStyles['window_title_text'];
		context2d.fillText(item.title, item.x + 5, item.y + 15 / 2);

		// Main background
		context2d.fillStyle = GUIfillStyles['window_background'];
		context2d.fillRect(item.x, item.y + 15, item.w, item.h - 15);
	}
}
