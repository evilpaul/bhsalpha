// List of handlers for the various resource types. Each handler is given a binary data blob which
// it must transform and return to the doneCallback
const resourceTypeHandlers = {
	'collisionMap': _handleCollisionMapResource,
	'font': _handleFontResource,
	'image': _handleImageResource,
};


// image => Image element
function _handleImageResource(binaryData, doneCallback) {
	// Create an Image element
	let image = new Image();
	image.onload = () => {
		// Return the Image data once it has loaded
		doneCallback(image);
	}
	image.src = 'data:image;base64,' + btoa([].reduce.call(binaryData, function (p, c) { return p + String.fromCharCode(c) }, ''));
}

// font => Font object
function _handleFontResource(binaryData, doneCallback) {
	// Create an Image element
	let image = new Image();
	image.onload = () => {
		// Use that Image to create a Font object once it has loaded
		doneCallback(new Font(image));
	}
	image.src = 'data:image;base64,' + btoa([].reduce.call(binaryData, function (p, c) { return p + String.fromCharCode(c) }, ''));
}

// collisionMap => CollisionMap object
function _handleCollisionMapResource(binaryData, doneCallback) {
	// Create an Image element
	let image = new Image();
	image.onload = () => {
		// Use that Image to create a CollisionMap object once it has loaded
		doneCallback(makeCollisionMap(image));
	}
	image.src = 'data:image;base64,' + btoa([].reduce.call(binaryData, function (p, c) { return p + String.fromCharCode(c) }, ''));
}
