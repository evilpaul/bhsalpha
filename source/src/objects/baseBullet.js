class BaseBulletObject extends BaseObject {
	constructor(xPos, yPos, direction, speed, spriteId, type) {
		super(xPos, yPos, spriteId, type)
		const radians = degToRad(direction);
		this.xVel = Math.sin(radians) * speed;
		this.yVel = -Math.cos(radians) * speed;
		this.isDead = false;
	}
	update() {
		super.update();

		this.xPos += this.xVel;
		this.yPos += this.yVel;
	}
	isAlive() {
		return !this.isDead && !(this.xPos < -this.image.width || this.xPos > globalConfig.canvasSize || this.yPos < -this.image.height || this.yPos > globalConfig.canvasSize);
	}
}
