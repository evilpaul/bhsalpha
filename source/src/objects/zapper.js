class ZapperObject extends BaseObject {
	static typeName () {
		return 'zapper';
	}
	static objectConfigProperties() {
		return {
			x: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
			y: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
			initialDelay: {type: 'integer', minimum:0, maximum:600, default:0},
			orientation: {type:'string', enum:['x', 'y'], default:'x'},
		};
	}

	constructor(config) {
		super(config.x, config.y, config.orientation + '_zapper_inactive', 'enemy')
		this.config = config;

		this.initialDelay = this.config.initialDelay;
		this.speed = 0;
		this.heading = null;
	}
	update() {
		super.update();

		// Delay the zapper until the countdown has reached zero
		if (this.animator.getCurrentAnimationName() == this.config.orientation + '_zapper_inactive') {
			if (this.initialDelay-- == 0) {
				// Now the zapper is active
				this.animator.set(this.config.orientation + '_zapper_move');
			} else {
				// No further update until it's active..
				return;
			}
		}

		// Caluclate heading and distance to player
		const dx = playerXCentre - this.xPos - this.image.width / 2;
		const dy = playerYCentre - this.yPos - this.image.height / 2;
		const desiredHeading = Math.atan2(dx, dy);
		const distance = length2d(dx, dy);

		// On the first update, snap to the desired heading instantly
		if (this.heading == null) {
			this.heading = desiredHeading;
		}

		if (this.animator.getCurrentAnimationName() == this.config.orientation + '_zapper_move') {
			// Calculate target speed based on distance to player, and lerp towards it
			const cappedDistance = Math.min(distance / globalConfig.zapper.maxSpeedDistance, 1);
			const targetSpeed = lerp(globalConfig.zapper.minSpeed, globalConfig.zapper.maxSpeed, Math.pow(cappedDistance, 2));
			this.speed = lerp(this.speed, targetSpeed, globalConfig.zapper.moveStateSpeedChange);

			// Lerp towards the desired heading. Amount of lerp is based on speed
			const headingLerpRate = lerp(globalConfig.zapper.maxHeadingChange, globalConfig.zapper.minHeadingChange, this.speed / globalConfig.zapper.maxSpeed);
			const diff = desiredHeading - this.heading;
			if (diff > Math.PI) this.heading += 2 * Math.PI;
			else if (diff < -Math.PI) this.heading -= 2 * Math.PI;
			this.heading += (desiredHeading - this.heading) * headingLerpRate;
		} else {
			// When the zapper is firing it just heads in the same direction and slows to a halt
			this.speed = lerp(this.speed, 0, globalConfig.zapper.fireStateSpeedChange);
		}

		// Move based on heading and speed
		this.xPos += Math.sin(this.heading) * this.speed;
		this.yPos += Math.cos(this.heading) * this.speed;

		// Limit to the bounds of the screen
		this.xPos = clamp(this.xPos, -this.image.width / 2, globalConfig.canvasSize - this.image.width / 2);
		this.yPos = clamp(this.yPos, -this.image.height / 2, globalConfig.canvasSize - this.image.height / 2);
	}
	getEditorProperties() {
		return ZapperObject.objectConfigProperties();
	}
}
