class SimpleBulletObject extends BaseBulletObject {
	constructor(xPos, yPos, direction) {
		super(xPos, yPos, direction, globalConfig.simpleBulletSpeed, 'simple_bullet', 'bullet')

		this.hasGrazed = false;
	}
	onCollision(otherType, mapType) {
		if (mapType == 'hit') {
			this.isDead = true;
		} else {
			if (!this.hasGrazed) {
				bulletsGrazed++;
				this.hasGrazed = true;
				this.animator.set('simple_bullet_grazed');
			}
			currentScore = currentScore + 1;
		}
	}
}
