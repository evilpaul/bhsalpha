var playerXCentre = 0;
var playerYCentre = 0;


class PlayerObject extends BaseObject {
	static typeName () {
		return 'player';
	}
	static objectConfigProperties() {
		return {
			x: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
			y: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
		};
	}

	constructor(config) {
		super(config.x, config.y, globalConfig.player.mainAnimationName, 'player');
		this.config = config;
		
		this.xVelocity = 0;
		this.yVelocity = 0;
	}
	update() {
		super.update();
		
		// Horizontal movement component
		if (inputManager.keysHeld.has('ArrowLeft')) {
			this.xVelocity = Math.max(this.xVelocity - globalConfig.player.acceleration, -globalConfig.player.maxSpeed);
		} else if (inputManager.keysHeld.has('ArrowRight')) {
			this.xVelocity = Math.min(this.xVelocity + globalConfig.player.acceleration, +globalConfig.player.maxSpeed);
		} else {
			if (this.xVelocity < 0) {
				this.xVelocity = Math.min(this.xVelocity + globalConfig.player.drag, 0);
			} else {
				this.xVelocity = Math.max(this.xVelocity - globalConfig.player.drag, 0);
			}
		}

		// Vertical movement component
		if (inputManager.keysHeld.has('ArrowUp')) {
			this.yVelocity = Math.max(this.yVelocity - globalConfig.player.acceleration, -globalConfig.player.maxSpeed);
		} else if (inputManager.keysHeld.has('ArrowDown')) {
			this.yVelocity = Math.min(this.yVelocity + globalConfig.player.acceleration, +globalConfig.player.maxSpeed);
		} else {
			if (this.yVelocity < 0) {
				this.yVelocity = Math.min(this.yVelocity + globalConfig.player.drag, 0);
			} else {
				this.yVelocity = Math.max(this.yVelocity - globalConfig.player.drag, 0);
			}
		}

		// Move based on velocity
		this.xPos += this.xVelocity;
		this.yPos += this.yVelocity;

		// Limit to the bounds of the screen
		this.xPos = clamp(this.xPos, 0, globalConfig.canvasSize - this.image.width);
		this.yPos = clamp(this.yPos, 0, globalConfig.canvasSize - this.image.height);

		// Store player position globally
		playerXCentre = this.xPos + this.image.width / 2;
		playerYCentre = this.yPos + this.image.height / 2;
	}
	onCollision(otherType, mapType) {
		if (mapType == 'hit') {
			this.animator.set(globalConfig.player.hitAnimationName);
			hits = hits + 1;
		}
	}
}
