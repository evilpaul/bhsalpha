class BaseObject {
	static schema() {
		// Gather the properties into an object schema
		let schema = {
			type: 'object',
			required: [],
			properties: {
				...this.objectConfigProperties()
			},
			additionalProperties: false,
		};

		// Mark all properties as required
		schema.required = Object.keys(schema.properties)

		return schema;
	}
	static defaultConfig() {
		const schema = this.schema();
		let defaultConfig = {};
		for (var property in schema.properties) {
			defaultConfig[property] = schema.properties[property].default;
		};
		return defaultConfig;
	}
	static typeName() {
		return null;
	}
	static objectConfigProperties() {
		return null;
	}

	constructor(xPos, yPos, animationName, group) {
		this.animator = new Animator(2);
		this.animator.set(animationName);
		
		const animationData = this.animator.get();
		this.image = resourceManager.getResource(animationData[0]);
		this.collisionMaps = animationData[1];

		this.xPos = xPos - this.image.width / 2;
		this.yPos = yPos - this.image.height / 2;
		this.group = group;
	}
	update() {
		this.animator.update();
		const animationData = this.animator.get();
		this.image = resourceManager.getResource(animationData[0]);
		this.collisionMaps = animationData[1];
	}
	isAlive() {
		return true;
	}
	render() {
		context2d.drawImage(this.image, this.xPos, this.yPos);	
	}
	onCollision(otherType, mapType) {
	}
	getEditorProperties() {
		return {
			x: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
			y: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
		};
	}

}
