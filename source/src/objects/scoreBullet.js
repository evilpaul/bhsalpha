class ScoreBulletObject extends BaseBulletObject {
	constructor(xPos, yPos, direction) {
		super(xPos, yPos, direction, globalConfig.scoreBulletSpeed, 'score_bullet_inactive', 'scoreBullet');
		
		this.activationTimer = globalConfig.scoreBulletActivationDelay;
	}
	update() {
		super.update();

		if (this.activationTimer > 0) {
			this.activationTimer--;
			if (this.activationTimer == 0) {
				this.animator.set('score_bullet_active');
			}
		}
	}
	onCollision(otherType, mapType) {
		currentScore = currentScore + 50;
		this.isDead = true;
	}
}
