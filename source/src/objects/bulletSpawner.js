class BulletSpawnerObject extends BaseObject {
	static typeName () {
		return 'bulletSpawner';
	}
	static objectConfigProperties() {
		return {
			x: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
			y: {type:'integer', minimum:0, maximum:globalConfig.canvasSize, default:0},
			spreadCount: {type:'integer', minimum:1, maximum:50, default:1},
			spreadAngle: {type:'integer', minimum:0, maximum:360, default:0},
			salvoCount: {type:'integer', minimum:1, maximum:10, default:1},
			salvoDelay: {type:'integer', minimum:0, maximum:60, default:10},
			rotationStep: {type:'integer', minimum:-360, maximum:360, default:5},
			initialDirection: {type:'integer', minimum:0, maximum:360, default:0},
			reverseCount: {type:'integer', minimum:0, maximum:100, default:0},
			initialDelay: {type:'integer', minimum:0, maximum:600, default:0},
			delay: {type:'integer', minimum:0, maximum:60, default:10},
			bulletType:  {type:'string', enum:['simple', 'score'], default:'simple'},
		};
	}

	constructor(config) {
		super(config.x, config.y, 'spawner_start', 'spawner')
		this.config = config;

		this.timer = this.config.initialDelay;
		this.salvoNumber = 0;
		this.salvoTimer = 0;
		this.direction = this.config.initialDirection;
		this.reverseCounter = this.config.reverseCount;
		this.directionModifier = +1;
		if (this.config.bulletType == 'simple') {
			this.spawnFunction = (xPos, yPos, direction) => new SimpleBulletObject(xPos, yPos, direction);
		} else if (this.config.bulletType == 'score') {
			this.spawnFunction = (xPos, yPos, direction) => new ScoreBulletObject(xPos, yPos, direction);
		} else {
			logManager.warning('Unknown bullet type "' + type + '" in spawner config. Defaulting to "simple"')
			this.spawnFunction = (xPos, yPos, direction) => new SimpleBulletObject(xPos, yPos, direction);
		}
	}
	update() {
		super.update();

		if (--this.timer <= 0) {
			if (--this.salvoTimer <= 0) {
				if (this.salvoNumber < this.config.salvoCount) {
					this.salvoNumber++;
					this.salvoTimer = this.config.salvoDelay;

					// Bullet origin is the middle of the spawner
					const xPos = this.xPos + this.image.width / 2;
					const yPos = this.yPos + this.image.height / 2;

					if (this.config.spreadCount == 1) {
						// There is only one bullet in the spread
						objectManager.add(this.spawnFunction(xPos, yPos, this.direction));
					} else {
						// There are multiple bullets in the spread
						const startAngle = this.direction - this.config.spreadAngle / 2;
						const spreadCount = this.config.spreadAngle == 360 ? this.config.spreadCount : this.config.spreadCount - 1;
						for (let i = 0; i < this.config.spreadCount; i++) {
							const direction = startAngle + i * this.config.spreadAngle / spreadCount; 
							objectManager.add(this.spawnFunction(xPos, yPos, direction));
						}
					}

					// Animate the image
					this.animator.set('spawner_fire');
				} else {
					// Reset, ready for next salvo
					this.salvoNumber = 0;
					this.savloTimer = 0;
					this.direction += this.config.rotationStep * this.directionModifier;
					this.timer = this.config.delay;

					// Deal with reversing direction
					if (this.config.reverseCount > 0) {
						if (--this.reverseCounter <= 1) {
							this.reverseCounter = this.config.reverseCount;
							this.directionModifier *= -1;
						}
					}
				}
			}
		}
	}
	getEditorProperties() {
		return BulletSpawnerObject.objectConfigProperties();
	}
}
