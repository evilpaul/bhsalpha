class LogManager {
	constructor() {
		this.logLines = [];
		this.outputElement = document.getElementById('debugText');
		this.info('LogManager started');
		if (!this.outputElement) {
			this.warning('Couldn\'t find debugText element on the page. Log messages will go to console only');
		}
	}
	info(message) {
		// Informational messages about how the system is operating
		this._log('INFO', message);
	}
	warning(message) {
		// Something went wrong, but the system handled it
		this._log('WARNING', message);
	}
	error(message) {
		// An entire operation failed, but was handled
		this._log('ERROR', message);
	}
	fatal(message) {
		// An error that means that we cannot continue
		this._log('FATAL', message);
		alert(message);
	}
	_log(severity, message) {
		// Add the severity to the start of the message
		const fullMessage = '[' + severity + '] ' + message;

		// Append to list and limit to 20 lines
		this.logLines.push(fullMessage);
		if (this.logLines.length > globalConfig.numOutputLinesFromLoggingManager) {
			this.logLines.shift();
		}

		// Output to the HTML element
		if (this.outputElement) {
			this.outputElement.innerHTML = '';
			this.logLines.forEach(line => {
				this.outputElement.innerHTML += line + "\n";
			});
		}

		// Echo to console too
		console.log(fullMessage);
	}
}
