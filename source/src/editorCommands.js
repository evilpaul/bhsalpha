class Command {
	constructor(type, config, previousState, newState, applyStateFunction) {
		this.type = type;
		this.config = config;
		this.previousState = previousState;
		this.newState = newState;
		this.applyStateFunction = applyStateFunction;
	}
	shouldDo() {
		return JSON.stringify(this.previousState, Object.keys(this.previousState).sort()) != JSON.stringify(this.newState, Object.keys(this.newState).sort());
	}
	do() {
		this.applyStateFunction(this.config, this.newState);
	}
	undo() {
		this.applyStateFunction(this.config, this.previousState);
	}
	fold(previousCommand) {
		this.previousState = previousCommand.previousState;
		return true;
	}
	debugText() {
		return this.type;
	}
}


class CommandSelect extends Command {
	constructor(levelEditor, objectId, selectionOffsetX, selectionOffsetY) {
		const config = {
			levelEditor: levelEditor,
		};
		const previousState = {
			objectId: levelEditor.currentSelectionId,
			selectionOffsetX: levelEditor.currentSelectionOffsetX,
			selectionOffsetY: levelEditor.currentSelectionOffsetY,
		};
		const newState = {
			objectId: objectId,
			selectionOffsetX: selectionOffsetX,
			selectionOffsetY: selectionOffsetY,
		};
		super('Select', config, previousState, newState, (config, state) => {
			config.levelEditor.currentSelectionId = state.objectId;
			config.levelEditor.currentSelectionOffsetX = state.selectionOffsetX;
			config.levelEditor.currentSelectionOffsetY = state.selectionOffsetY;
		});
	}
	fold(previousCommand) {
		return false;
	}
	debugText() {
		return this.newState.objectId ? 'Select ' + this.newState.objectId : 'Deselect';
	}
}


class CommandMove extends Command {
	constructor(objectManager, objectId, x, y, w, h) {
		const config = {
			objectManager: objectManager,
			objectId: objectId
		};
		const [oldX, oldY] = objectManager.getObjectConfigPosition(objectId);
		const previousState = {
			x: oldX - w / 2,
			y: oldY - h / 2,
		};
		const newState = {
			x: x,
			y: y,
		};
		super('Move', config, previousState, newState, (config, state) => {
			config.objectManager.setObjectPosition(config.objectId, state.x, state.y);
		});
	}
	debugText() {
		return 'Move ' + this.config.objectId;
	}
}


class CommandConfig extends Command {
	constructor(objectManager, objectId, newConfigData) {
		let [object, group] = objectManager.findObject(objectId);
		const config = {
			objectManager: objectManager,
			objectConstructor: object.__proto__.constructor,
			objectId: objectId
		};
		const previousState = JSON.parse(JSON.stringify(object.config));
		const newState = JSON.parse(JSON.stringify(newConfigData));
		super('Config', config, previousState, newState, (config, state) => {
			// Perform a hard update of the config - delete and re-create with the new config
			config.objectManager.delete(config.objectId);
			config.objectManager.add(new config.objectConstructor(state), config.objectId);
		});
	}
	debugText() {
		return 'Config ' + this.config.objectId;
	}
}


class CommandDelete extends Command {
	constructor(levelEditor, objectManager, objectId) {
		let [object, group] = objectManager.findObject(objectId);
		const config = {
			levelEditor: levelEditor,
			objectManager: objectManager,
			objectConstructor: object.__proto__.constructor,
			objectConfig: object.config,
			objectId: objectId,
		};
		const previousState = {
			exists: true,
			selectionId: levelEditor.currentSelectionId,
		};
		const newState = {
			exists: false,
			selectionId: null,
		};
		super('Delete', config, previousState, newState, (config, state) => {
			if (!state.exists) {
				config.objectManager.delete(config.objectId);
			} else {
				config.objectManager.add(new config.objectConstructor(config.objectConfig), config.objectId);
			}
			config.levelEditor.currentSelectionId = state.selectionId;
		});
	}
	fold(previousCommand) {
		return false;
	}
	debugText() {
		return 'Delete ' + this.config.objectId;
	}
}


class CommandManager {
	constructor() {
		this.commandStack = [];
		this.redoStack = [];
		this.lastCommandTimeInMs = 0;
		this.autoFoldCutOffTimeInMs = 1000;
	}
	doCommand(command) {
		// See if the command is worth doing? ie: does it change any state?
		if (command.shouldDo()) {
			// It is. Empty the redo stack
			this.redoStack = [];

			// Execute the command
			command.do();

			// See if we can fold the command into the last executed command
			const newTimeInMs = new Date().getTime();
			if (this.commandStack.length) {
				// Is the last command recent enough to consider folding?
				if (newTimeInMs < this.lastCommandTimeInMs + this.autoFoldCutOffTimeInMs) {
					// Is the last command of the same type?
					const previousCommand = this.commandStack[this.commandStack.length - 1];
					if (previousCommand.type == command.type) {
						// Finally, let the command try to perform the fold
						if (command.fold(previousCommand)) {
							// Fold was successful. Remove the old command - the new one will replace it
							this.commandStack.pop();
						}
					}
				}
			}

			// Put the new command onto the command stack
			this.commandStack.push(command);
			this.lastCommandTimeInMs = newTimeInMs;
		}
	}
	undo() {
		if (this.commandStack.length) {
			let command = this.commandStack.pop();
			command.undo();
			this.redoStack.push(command);
		}
	}
	redo() {
		if (this.redoStack.length) {
			let command = this.redoStack.pop();
			command.do();
			this.commandStack.push(command);
		}
	}
	getDebugText() {
		let text = [];
		this.commandStack.forEach((command) => {
			text.push(command.debugText());
		});
		if (text.length > 10) {
			text = text.slice(-10);
			text.unshift('...');
		}
		text.push('>>>');
		this.redoStack.slice().reverse().forEach((command) => {
			text.push(command.debugText());
		});
		if (text.length > 20) {
			text = text.slice(0, 20);
			text.push('...');
		}
		return text;
	}
}
