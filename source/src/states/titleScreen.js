class TitleScreenState extends BaseState {
	constructor() {
		super('TitleScreen');
	}
	enter() {
		this.readyToExit = false;
		this.frameCount = 0;
	}
	update() {
		this.frameCount++;

		if (inputManager.keysPressed.has('1')) {
			chosenLevel = 0;
			this.readyToExit = true;			
		} else if (inputManager.keysPressed.has('2')) {
			chosenLevel = 1;
			this.readyToExit = true;			
		}
	}
	render() {
		// Draw background
		context2d.fillStyle = '#000';
		context2d.fillRect(0, 0, canvas.width, canvas.height);

		// Render titles
		const font = resourceManager.getResource('font');
		let message = 'Bullet Hell Survive Alpha\nbhsalpha.blogspot.com\n\n\nArrow keys to move';
		if (this.frameCount & 20) {
			message += '\n\n\n\n\n>> Press 1 or 2 <<\n>> to select level <<';
		}
		font.print(globalConfig.canvasSize / 2, 100, message, FONT_XALIGN.middle, FONT_YALIGN.top);
	}
	isReadyToExit() {
		return this.readyToExit;
	}
	exit() {
		// Set up the next state
		stateManager.pushState(new GameState());
	}
}
