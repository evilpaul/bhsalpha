class LoadingState extends BaseState {
	constructor() {
		super('Loading');
	}
	enter() {
	}
	render() {
		// Draw background
		context2d.fillStyle = '#000';
		context2d.fillRect(0, 0, canvas.width, canvas.height);

		// Show loading message as soon as the font resource is available
		if (resourceManager.isResourceAvailable('font')) {
			resourceManager.getResource('font').print(globalConfig.canvasSize / 2, globalConfig.canvasSize / 2, 'Loading', FONT_XALIGN.middle, FONT_YALIGN.middle);
		}
	}
	isReadyToExit() {
		// Exit when resource loading is complete
		return resourceManager.areAllResourcesLoaded();
	}
	exit() {
		// Set up the next state
		stateManager.pushState(new LevelEditorState());
	}
}
