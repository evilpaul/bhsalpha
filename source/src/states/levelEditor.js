const testEditorLevel = {
	type: 'BHS Map',
	version: 1,
	meta: {
		name: 'THIS IS A TEST LEVEL',
		targetScore: 1000,
		targetHits: 2,
	},
	objects: {
		player: [
			{ x: 100, y: 100 },
		],
		zapper: [
			{ x: 400, y: 400 , initialDelay: 60, orientation: 'y' },
		],
		bulletSpawner: [
			{ x: 50, y: 50, spreadCount: 9, spreadAngle: 90, salvoCount: 2, salvoDelay: 5, rotationStep: 10, initialDirection: 90, reverseCount: 10, initialDelay: 0, delay: 45, bulletType: 'simple' },
			{ x: 320, y: 320, spreadCount: 4, spreadAngle: 360, salvoCount: 1, salvoDelay: 0, rotationStep: 15, initialDirection: 90, reverseCount: 0, initialDelay: 0, delay: 10, bulletType: 'score' },
		],
	}
}


class LevelEditorState extends BaseState {
	constructor() {
		super('LevelEditor');

		this.gui = new GUI(canvas, context2d);
		this.guiVisible = true;
		this.commandManager = new CommandManager();
	}
	enter() {
		// Load level
		this.levelLoader = new LevelLoader();
		if (!this.levelLoader.load(testEditorLevel)) {
			logManager.error('Level loading failed!');
		}

		// Start with the game world paused
		this.updateActive = false;

		// Details about objects
		this.currentSelectionId = null;
		this.currentSelectionOffsetX = 0;
		this.currentSelectionOffsetY = 0;
	}
	update() {
		// Command undo/redo
		if (inputManager.keysHeld.has('Control')) {
			if (inputManager.keysPressed.has('z')) {
				this.commandManager.undo();
			} else if (inputManager.keysPressed.has('y')) {
				this.commandManager.redo();
			}
		}

		// Toggle the GUI
		if (inputManager.keysPressed.has('`')) {
			this.guiVisible = !this.guiVisible;
		}

		// Begin the GUI
		this.gui.startFrame();
		let editorWindowClosed = false;
		if (this.guiVisible) {
			// If there is a current selection then show an edit window on the GUI
			if (this.currentSelectionId) {
				// Gather data about the selected object
				const [object, group] = objectManager.findObject(this.currentSelectionId);
				const editorProperties = object.getEditorProperties();

				// Take a local copy of the object's config data
				let editedData = JSON.parse(JSON.stringify(object.config));

				// Show the edit window
				if (this.gui.beginWindow('Edit Object', true) == 'closed') {
					editorWindowClosed = true;
				}
				for (const propertyName in editorProperties) {
					const propertyDetails = editorProperties[propertyName];
					if (propertyDetails.type == 'integer') {
						editedData[propertyName] = this.gui.intValue(propertyName, editedData[propertyName], propertyDetails.minimum, propertyDetails.maximum);
					} else if (propertyDetails.type == 'string' && 'enum' in propertyDetails) {
						editedData[propertyName] = this.gui.listValue(propertyName, editedData[propertyName], propertyDetails.enum);
					} else {
						this.gui.label(propertyName);
					}
				}
				this.gui.endWindow();

				// Any data changed?
				if (!Object.keys(object.config).every(key => object.config[key] === editedData[key])) {
					if (object.config.x != editedData.x || object.config.y != editedData.y) {
						// Deal with position as a special case
						this.commandManager.doCommand(new CommandMove(objectManager, this.currentSelectionId, editedData.x - object.image.width / 2, editedData.y - object.image.height / 2, object.image.width, object.image.height));
					} else {
						// All other properties
						this.commandManager.doCommand(new CommandConfig(objectManager, this.currentSelectionId, editedData));
					}
				}
			}

			// Show editor information window
			this.gui.beginWindow('Editor Info');

			// Instructions
			if (this.gui.beginGroup('Instructions', false)) {
				this.gui.label('SPACE to pause/unpause game');
				this.gui.label('` to hide/show GUI');
				this.gui.label('DELETE to delete selected object');
				this.gui.label('CTRL+Z/CTRL+Y to undo/redo actions');
			}
			this.gui.endGroup();

			// Undo/redo stack
			if (this.gui.beginGroup('Undo/Redo Stack', false)) {
				const editorCommandList = this.commandManager.getDebugText();
				editorCommandList.forEach(command => {
					this.gui.label(command);
				});
			}
			this.gui.endGroup();

			this.gui.endWindow();
		}

		// Finished with the GUI
		// Only allow other interaction if the GUI is hidden or didn't consume the input
		const allowUserInteraction = !this.guiVisible || !this.gui.wasInputConsumed();

		// Press space to pause/unpause the game world
		if (this.gui.hotkey(' ')) {
			this.updateActive = !this.updateActive;	
		}

		// Update the game world
		if (this.updateActive) {
			objectManager.update();
			objectManager.collide('player', 'enemy', 'hit');
			objectManager.collide('player', 'bullet', 'hit');
			objectManager.collide('player', 'bullet', 'score');
			objectManager.collide('player', 'scoreBullet', 'score');
		}

		// De-select objects if mouse or escape pressed or the editor window was closed
		let newSelectedId = this.currentSelectionId;
		let newSelectedOffsetX = 0;
		let newSelectedOffsetY = 0;
		if ((allowUserInteraction && inputManager.mousePressed) || this.gui.hotkey('Escape') || editorWindowClosed) {
			newSelectedId = null;
		}

		// Get selection boxes for all game object
		const objectDetails = objectManager.getDetailsForAllObjectOfGroup(['spawner', 'player', 'enemy']);

		// Create render list for object selection boxes
		const mouseX = inputManager.mouseX;
		const mouseY = inputManager.mouseY;
		this.objectSelectionBoxes = [];
		objectDetails.forEach(objectDetail => {
			// Make the selection box slightly larger
			const x = objectDetail.x - 5;
			const y = objectDetail.y - 5;
			const w = objectDetail.w + 10;
			const h = objectDetail.h + 10;

			// See if the mouse is over this box
			const mouseOver = allowUserInteraction  && (mouseX >= x && mouseX < x + w && mouseY >= y && mouseY < y + h);
			if (mouseOver && inputManager.mousePressed) {
				newSelectedId = objectDetail.id;
				newSelectedOffsetX = objectDetail.x - mouseX;
				newSelectedOffsetY = objectDetail.y - mouseY;
			}

			// Add an entry to the render list so that we can see the object highlight
			if (this.currentSelectionId == objectDetail.id) {
				// Outline
				this.objectSelectionBoxes.push({
					x: x-2,
					y: y-2,
					w: w+4,
					h: h+4,
					fillStyle: '#fa3'
				});
				// Object
				this.objectSelectionBoxes.push({
					x: x,
					y: y,
					w: w,
					h: h,
					fillStyle: mouseOver ? '#888' : '#555'
				});
			} else {
				// Object
				this.objectSelectionBoxes.push({
					x: x,
					y: y,
					w: w,
					h: h,
					fillStyle: mouseOver ? '#252' : '#522'
				});
			}
		});

		// Has selection changed?
		if (newSelectedId != this.currentSelectionId) {
			this.commandManager.doCommand(new CommandSelect(this, newSelectedId, newSelectedOffsetX, newSelectedOffsetY));
		}

		// User operations on selected object
		if (allowUserInteraction && this.currentSelectionId) {
			if (inputManager.mouseHeld) {
				// Move with the mouse
				const [object, group] = objectManager.findObject(this.currentSelectionId);
				this.commandManager.doCommand(new CommandMove(objectManager, this.currentSelectionId, inputManager.mouseX + this.currentSelectionOffsetX, inputManager.mouseY + this.currentSelectionOffsetY, object.image.width, object.image.height));
			} else if (inputManager.keysPressed.has('Delete')) {
				// Delete
				let [object, group] = objectManager.findObject(this.currentSelectionId);
				if (group != 'player') {
					this.commandManager.doCommand(new CommandDelete(this, objectManager, this.currentSelectionId));
				}
			}
		}
	}
	render() {
		// Draw background
		context2d.fillStyle = this.updateActive ? '#111' : '#300';
		context2d.fillRect(0, 0, canvas.width, canvas.height);

		// Draw selection boxes around each object
		this.objectSelectionBoxes.forEach(objectBox => {
			context2d.fillStyle = objectBox.fillStyle;
			context2d.fillRect(objectBox.x, objectBox.y, objectBox.w, objectBox.h);
		});

		// Draw objects
		objectManager.render();

		// Render the GUI
		this.gui.renderFrame();
	}
	isReadyToExit() {
		return false;
	}
	exit() {
		// Remove all objects
		objectManager.reset();
	}
}
