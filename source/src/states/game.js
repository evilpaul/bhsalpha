const testLevels = [
	// First test level
	{
		type: 'BHS Map',
		version: 1,
		meta: {
			name: 'THIS IS A TEST LEVEL',
			targetScore: 1000,
			targetHits: 2,
		},
		objects: {
			player: [
				{ x: 100, y: 100 },
			],
			zapper: [
				{ x: 400, y: 400 , initialDelay: 60, orientation: 'y' },
			],
			bulletSpawner: [
				{ x: 50, y: 50, spreadCount: 9, spreadAngle: 90, salvoCount: 2, salvoDelay: 5, rotationStep: 10, initialDirection: 90, reverseCount: 10, initialDelay: 0, delay: 45, bulletType: 'simple' },
				{ x: 320, y: 320, spreadCount: 4, spreadAngle: 360, salvoCount: 1, salvoDelay: 0, rotationStep: 15, initialDirection: 90, reverseCount: 0, initialDelay: 0, delay: 10, bulletType: 'score' },
			],
		}
	},
	// Second test level
	{
		type: 'BHS Map',
		version: 1,
		meta: {
			name: 'THIS IS ANOTHER TEST LEVEL',
			targetScore: 800,
			targetHits: 1,
		},
		objects: {
			player: [
				{ x: 320, y: 600 },
			],
			bulletSpawner: [
				{ x: 40, y: 100, spreadCount: 15, spreadAngle: 360, salvoCount: 3, salvoDelay: 5, rotationStep: 2, initialDirection: 0, reverseCount: 10, initialDelay: 0, delay: 25, bulletType: 'simple' },
				{ x: 600, y: 100, spreadCount: 15, spreadAngle: 360, salvoCount: 3, salvoDelay: 5, rotationStep: -2, initialDirection: 0, reverseCount: 10, initialDelay: 0, delay: 25, bulletType: 'simple' },
			],
		}
	}
];



GAME_STATES = makeEnum({
	intro: 0,
	game: 1,
	outro: 2,
	exiting: 3,
});


class GameState extends BaseState {
	constructor() {
		super('Game');
	}
	enter() {
		// Initalise state manager
		this.state = GAME_STATES.intro;
		this.stateTimer = 0;

		// Reset game counters
		currentScore = 0;
		bulletsGrazed = 0;
		hits = 0;

		// Background colours
		this.backgroundGradient = context2d.createRadialGradient(globalConfig.canvasSize / 2, globalConfig.canvasSize / 2, globalConfig.canvasSize * 0.2, globalConfig.canvasSize / 2, globalConfig.canvasSize / 2, globalConfig.canvasSize * 0.6);
		this.backgroundGradient.addColorStop(0, '#001');
		this.backgroundGradient.addColorStop(1, '#113');

		// Animator object for the intro countdown animation
		this.introAnimator = new Animator(1);
		this.introAnimator.set('level_intro_countdown');

		// Load level
		this.levelLoader = new LevelLoader();
		if (!this.levelLoader.load(testLevels[chosenLevel])) {
			logManager.error('Level loading failed!');
		}
	}
	update() {
		this.stateTimer++;

		// State specific updates
		switch (this.state) {
			case GAME_STATES.intro:
				// Update animation and exit this state when it has finished
				this.introAnimator.update();
				if (this.introAnimator.getCurrentAnimationName() == null) {
					// Next game state
					this.state = GAME_STATES.game;
					this.stateTimer = 0;
				}
				break;

			case GAME_STATES.game:
				// Update and collied
				objectManager.update();
				objectManager.collide('player', 'enemy', 'hit');
				objectManager.collide('player', 'bullet', 'hit');
				objectManager.collide('player', 'bullet', 'score');
				objectManager.collide('player', 'scoreBullet', 'score');

				// Exit when state has run for long enough
				if (this.stateTimer == globalConfig.framesPerSecond * globalConfig.level.gameLengthInSeconds) {
					// Next game state
					this.state = GAME_STATES.outro;
					this.stateTimer = 0;
				}
				break;

			case GAME_STATES.outro:
				// Exit when state has run for long enough
				if (this.stateTimer == globalConfig.framesPerSecond * globalConfig.level.outroLengthInSeconds) {
					// Next game state
					this.state = GAME_STATES.exiting;
					this.stateTimer = 0;
				}
				break;

			default:
				logManager.error('Unexpected state: ' + this.state);
		}
	}
	render() {
		// Draw background
		context2d.fillStyle = this.backgroundGradient;
		context2d.fillRect(0, 0, canvas.width, canvas.height);

		// Draw time bar
		const width = 400;
		const x = (globalConfig.canvasSize - width) / 2;
		const height = 20;
		const bottomBorder = 10;
		const y = globalConfig.canvasSize - bottomBorder - height;
		let alpha = 0.0;
		switch (this.state) {
			case GAME_STATES.intro:
				alpha = 0.0;
				break;

			case GAME_STATES.game:
				alpha = this.stateTimer / (globalConfig.framesPerSecond * globalConfig.level.gameLengthInSeconds);
				break;

			case GAME_STATES.outro:
				alpha = 1.0;
				break;
		}
		this.drawHorizontalBar(x, y, width, height, 2.0, 1.0 - alpha);

		// Draw objects
		objectManager.render();

		// State specific rendering
		let font = resourceManager.getResource('font');
		switch (this.state) {
			case GAME_STATES.intro:
				// Grey out objects in the background
				context2d.fillStyle = '#333a';
				context2d.fillRect(0, 0, canvas.width, canvas.height);

				// Draw countdown animation
				const imageName = this.introAnimator.get()[0];
				if (imageName) {
					const image = resourceManager.getResource(imageName);
					context2d.drawImage(image, (globalConfig.canvasSize - image.width) / 2, (globalConfig.canvasSize - image.height) / 2);
				}

				// Show level name
				font.print(globalConfig.canvasSize / 2, 400,  'Now entering:\n' + this.levelLoader.levelName, FONT_XALIGN.middle);
				break;

			case GAME_STATES.game:
				// Show in-game UI
				const totalBulletCount = objectManager.objectLists['bullet'].length + objectManager.objectLists['scoreBullet'].length
				font.print(10, 10,  'Hits: ' + hits + '\nBullets grazed: ' + bulletsGrazed + '\nScore: ' + currentScore + '\nBullet count: ' + totalBulletCount);
				break;

			case GAME_STATES.outro:
				// Grey out objects in the background
				context2d.fillStyle = '#333a';
				context2d.fillRect(0, 0, canvas.width, canvas.height);

				// Level summary text
				const textLines = [ 
					this.levelLoader.levelName,
					'',
					'',
					'Hits: ' + hits,
					'Target: ' + this.levelLoader.targetHits,
					hits <= this.levelLoader.targetHits ? 'PASSED' : 'FAILED',
					'',
					'Score: ' + currentScore,
					'Target: ' + this.levelLoader.targetScore,
					currentScore >= this.levelLoader.targetScore ? 'PASSED' : 'FAILED',
					'',
					'Bullets grazed: ' + bulletsGrazed,
				];

				// Reveal text over time
				const textLinesToDisplay = textLines.splice(0, this.stateTimer / 10);
				font.print(globalConfig.canvasSize / 2, globalConfig.canvasSize / 2, textLinesToDisplay.join('\n'), FONT_XALIGN.middle, FONT_YALIGN.middle);
				break;

			default:
				logManager.error('Unexpected state: ' + this.state);
		}
	}
	isReadyToExit() {
		return this.state == GAME_STATES.exiting;
	}
	exit() {
		// Remove all objects
		objectManager.reset();

		// Set up the next state
		stateManager.pushState(new TitleScreenState());
	}
	drawHorizontalBar(x, y, width, height, borderSize, alpha) {
		// Draw border
		context2d.fillStyle = '#666';
		context2d.fillRect(x, y, width, height);
		context2d.fillStyle = '#000';
		context2d.fillRect(x + borderSize, y + borderSize, width - borderSize * 2, height - borderSize * 2);

		// Draw inner bar
		const innerWidth = width - borderSize * 4;
		const midPoint = lerp(0, innerWidth, alpha);
		context2d.fillStyle = '#600';
		context2d.fillRect(x + borderSize * 2, y + borderSize * 2, width - borderSize * 4, height - borderSize * 4);
		context2d.fillStyle = '#060';
		context2d.fillRect(x + borderSize * 2, y + borderSize * 2, midPoint, height - borderSize * 4);
	}
}
