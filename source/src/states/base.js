class BaseState {
	constructor(name) {
		this.name = name;
	}
	enter() {
	}
	update() {
	}
	render() {
	}
	isReadyToExit() {
		return false;
	}
	exit() {
	}
}
