function degToRad(angle) {
  return angle * (Math.PI / 180);
}


function clamp(value, min, max) {
	return value <= min ? min : value >= max ? max : value;
}


function lerp(start, end, amount) {
	return (1 - amount) * start + amount * end;
}


function length2d(x, y) {
	return Math.sqrt((x * x) + (y * y));
}



function disallowUndefinedProperties(object) {
	const handler = {
		get(target, property) {
			if (property in target) {
				return target[property];
			}
			throw new Error('Property \'' + property + '\' is not defined');
		}
	};
	return new Proxy(object, handler);
}



class Enum {
	constructor(object) {
		for (const key in object) {
			this[key] = object[key];
		}
		return Object.freeze(this);
	}
}
function makeEnum(object) {
	return disallowUndefinedProperties(new Enum(object));
}

