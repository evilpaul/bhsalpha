const FONT_XALIGN = makeEnum({
	left: 0,
	middle: 1,
	right: 2,
});

const FONT_YALIGN = makeEnum({
	top: 0,
	middle: 1,
	bottom: 2,
});



class Font {
	constructor(image, charsWide=16, xSpacing=4, ySpacing=4) {
		this.image = image;
		this.charsWide = charsWide;
		this.charWidth = image.width / charsWide;
		this.charHeight = image.height / (128 / charsWide);
		this.xSpacing = xSpacing;
		this.ySpacing = ySpacing;
	}
	print(x, y, text, xAlign=FONT_XALIGN.left, yAlign=FONT_YALIGN.top) {
		const extents = this.getExtents(text);

		let xStart = x;
		if (yAlign == FONT_YALIGN.middle) {
			y -= extents.totalHeight / 2;
		} else if (yAlign == FONT_YALIGN.bottom) {
			y -= extents.totalHeight;
		}

		let firstCharOnLine = true;
		let currentLineNumber = 0;
		for (var i = 0; i < text.length; i++) {
			if (firstCharOnLine) {
				x = xStart;
				if (xAlign == FONT_XALIGN.middle) {
					x -= extents.totalWidth / 2;
					x += (extents.totalWidth - extents.lineWidths[currentLineNumber]) / 2;
				} else if (xAlign == FONT_XALIGN.right) {
					x -= extents.totalWidth;
					x += extents.totalWidth - extents.lineWidths[currentLineNumber];
				}
				firstCharOnLine = false;
			}

			const thisChar = text.charCodeAt(i);
			if (thisChar != 10) {
				// Normal character
				const sx = (thisChar % this.charsWide) * this.charWidth;
				const sy = (Math.floor(thisChar / this.charsWide)) * this.charHeight;  
				context2d.drawImage(this.image, sx, sy, this.charWidth, this.charHeight, x, y, this.charWidth, this.charHeight);
				x += this.charWidth + this.xSpacing;
			} else {
				// Newline
				firstCharOnLine = true;
				currentLineNumber++;
				y += this.charHeight + this.ySpacing;
			}
		}
	}
	getExtents(text) {
		var lineWidths = [];
		var charsInCurrentLine = 0;

		for (var i = 0; i < text.length; i++) {
			const thisChar = text.charCodeAt(i);
			if (thisChar != 10) {
				// Normal character
				charsInCurrentLine++;
			} else {
				// Newline
				const currentLineWidth = charsInCurrentLine * this.charWidth + (charsInCurrentLine - 1) * this.xSpacing;
				lineWidths.push(currentLineWidth);
				charsInCurrentLine = 0;
			}			
		}
		if (charsInCurrentLine > 0) {
			const currentLineWidth = charsInCurrentLine * this.charWidth + (charsInCurrentLine - 1) * this.xSpacing;
			lineWidths.push(currentLineWidth);
		}

		const totalLines = lineWidths.length;
		const totalWidth = Math.max(...lineWidths);
		const totalHeight = totalLines * this.charHeight + (totalLines - 1) * this.ySpacing;

		return {
			lineWidths: lineWidths,
			totalHeight: totalHeight,
			totalWidth: totalWidth,
		}
	}
}