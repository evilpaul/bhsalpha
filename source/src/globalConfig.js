globalConfig = makeEnum({
	// Size of the main game canvas
	canvasSize: 640,

	// Timing constants
	framesPerSecond: 60,
	msPerFrame: 1000 / 60,

	// Game state configuration
	level: {
		gameLengthInSeconds: 30,
		outroLengthInSeconds: 4,
	},

	// Player config
	player:
	{
		maxSpeed: 2.5,
		acceleration: 0.5,
		drag: 0.25,
		mainAnimationName: 'player',
		hitAnimationName: 'player_hit',
	},

	// Bullet config
	simpleBulletSpeed: 2,
	scoreBulletSpeed: 3,
	scoreBulletActivationDelay: 30,

	// Enemies
	zapper: {
		minSpeed: 0.1,
		maxSpeed: 4,
		maxSpeedDistance: 100,
		moveStateSpeedChange: 0.1,
		fireStateSpeedChange: 0.05,
		maxHeadingChange: 1.0,
		minHeadingChange: 0.1,
	},

	// System internals
	numOutputLinesFromLoggingManager: 10,
});
