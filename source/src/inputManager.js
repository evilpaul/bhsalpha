class InputManager {
	constructor() {
		// These are the properties that other systems can query
		this.keysHeld = new Set();
		this.keysPressed = new Set();
		this.keysReleased = new Set();
		this.mouseX = 0;
		this.mouseY = 0;
		this.mouseHeld = false;
		this.mousePressed = false;
		this.mouseReleased = false;

		// Store input events for each frame, to be processed in update()
		this.events = [];

		// Add handlers for input events
		document.addEventListener('keydown', (event) => {
			this.events.push({type:'keyDown', data:{code:event.key}});
		});
		document.addEventListener('keyup', (event) => {
			this.events.push({type:'keyUp', data:{code:event.key}});
		});
		document.addEventListener('mousemove', (event) => {
			const styling = getComputedStyle(canvas, null);
			const leftBorder = parseInt(styling.getPropertyValue('border-left-width'));
			const topBorder = parseInt(styling.getPropertyValue('border-top-width'));
			const clientRect = canvas.getBoundingClientRect();
			this.events.push({type:'mouseMove', data:{x:event.clientX - clientRect.left - leftBorder, y:event.clientY - clientRect.top - topBorder}});
		});
		document.addEventListener('mousedown', (event) => {
			this.events.push({type:'mouseDown', data:{}});
		});
		document.addEventListener('mouseup', (event) => {
			this.events.push({type:'mouseUp', data:{}});
		});
	}
	update() {
		// Process this frame's events
		this.keysPressed = new Set();
		this.keysReleased = new Set();
		this.mousePressed = false;
		this.mouseReleased = false;
		if (this.events.length) {
			this.events.forEach((event) => {
				switch (event.type) {
					case 'keyDown':
						this.keysHeld.add(event.data.code);
						this.keysPressed.add(event.data.code);
						break;

					case 'keyUp':
						this.keysHeld.delete(event.data.code);
						this.keysReleased.add(event.data.code);
						break;

					case 'mouseMove':
						this.mouseX = event.data.x;
						this.mouseY = event.data.y;
						break;

					case 'mouseDown':
						this.mousePressed = !this.mouseHeld;
						this.mouseHeld = true;
						break;

					case 'mouseUp':
						this.mouseReleased = true;
						this.mouseHeld = false;
						break;

					default:
						console.warning('Unexpected input event type: ' + event.type);
						break;
				}
			})

			// Reset events queue
			this.events = [];
		}
	}
}
