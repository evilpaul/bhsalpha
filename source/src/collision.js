function makeCollisionMap(image) {
	let tempCanvas = document.createElement('canvas');
	tempCanvas.width = image.width;
	tempCanvas.height = image.height;
	let tempContext = tempCanvas.getContext('2d');
	tempContext.drawImage(image, 0, 0);

	const imageData = tempContext.getImageData(0, 0, tempCanvas.width, tempCanvas.height).data;
	let data = new Uint8Array(tempCanvas.width * tempCanvas.height);
	for (let i = 0; i < imageData.length; i += 4) {
		const alpha = imageData[i + 3];
		data[i>>2] = alpha == 0 ? 0 : 255;
	}

	return { 
		width: tempCanvas.width,
		height: tempCanvas.height,
		data: data
	};
}


function isColliding(xA, yA, collisionMapA, xB, yB, collisionMapB) {
	// Quick and simple box collision
	if (xA + collisionMapA.width >= xB &&
		xA < xB + collisionMapB.width &&
		yA + collisionMapA.height >= yB &&
		yA < yB + collisionMapB.height) {

		// If that indicates a collision then use the collision maps
		for (let y = 0; y < collisionMapA.height; y++) {
			const yOther = Math.floor(yA) - Math.floor(yB) + y;
			for (let x = 0; x < collisionMapA.width; x++) {
				const bitThis = collisionMapA.data[x + y * collisionMapA.width];
				const xOther = Math.floor(xA) - Math.floor(xB) + x;
				if (xOther >= 0 && xOther < collisionMapB.width &&
					yOther >= 0 && yOther < collisionMapB.height) {
					var bitOther = collisionMapB.data[xOther + yOther * collisionMapB.width];
					if (bitThis & bitOther) {
						// Collision detected
						return true;
					}
				}
			}
		}
	}

	return false;
}
