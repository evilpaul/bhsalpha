#!/usr/bin/env python3

"""
Simple script to serve up the source directory on localhost. This lets you test the game on your
local machine without having to copy it to a webserver first. When the script is running you can
browse to http://localhost:8000 to run the game

You will need to have Python3 installed on your computer

The script should run without any trouble from Linux. In Windows you may be able to just double-click
on it to get it to work. If it doesn't you will first need to associate Python files with Python.
To do this, right-click on the file in Explorer, select "Open with" and choose Python.
"""


import http.server
import os
import socketserver

PORT = 8000
DIRECTORY = 'source'

os.chdir(DIRECTORY)
with socketserver.TCPServer(('', PORT), http.server.SimpleHTTPRequestHandler) as httpd:
    print('Bullet Hell Survive Alpha\n\nServing from %r at http://localhost:%s' % (DIRECTORY, PORT))
    httpd.serve_forever()
